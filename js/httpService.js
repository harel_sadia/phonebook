
/* global PhoneModel */

/* service that handle all the http request*/
var HttpService = {
  onSearch: function () {   //Search in phone book
    var formDetails = ['firstName', 'lastName', 'phone', 'email', 'orderBy'];
    var searchObj = PhoneModel.getFormDetails(formDetails);
    console.log(searchObj);
    $.ajax({
      url: 'php/phoneBook.php',
      type: 'GET',
      data: searchObj,
      error: function () {
      },
      success: function (resp) {
        console.log(resp);
        var response = JSON.parse(resp);
        PhoneModel.printPhoneBook(response.data);
      }
    });
  },
  getPhoneDetails: function (id) {   //Get row from phone book by id
    $.ajax({
      url: 'php/phoneBook.php',
      type: 'GET',
      data: {id: id},
      error: function () {
      },
      success: function (response) {
        console.log(response);
        sessionStorage.setItem('updatePhoneData', response); // save the row for update
        window.location.href = 'phoneUpdate.html';
      }
    });
  },
  updatePhone: function () {   //add/update row in phone book
    var details = ['firstName', 'lastName', 'phone', 'email', 'id'];

    var obj = PhoneModel.getFormDetails(details);
    $.ajax({
      url: 'php/editPhone.php',
      type: 'POST',
      data: obj,
      error: function () {
      },
      success: function (resp) {
        var response = JSON.parse(resp);
        console.log(response);
        if (response.data.result) {
          window.location.href = 'index.html';
        } else {
          alert('An error has occurred!');
        }
      }
    });
  }
};