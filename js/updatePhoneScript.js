
/* global PhoneModel, HttpService */

$(document).ready(function () {
  $('#id').val(0);
  var updatePhoneData = sessionStorage.getItem('updatePhoneData');
  if (updatePhoneData !== null) { //Check if it new or old row
    PhoneModel.fillFormDetails();
  }
});

