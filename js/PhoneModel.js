/* global HttpService */

var formDetails = ['firstName', 'lastName', 'phone', 'email'];

/* Class for all the business logic of phone book */
var PhoneModel = {
  printPhoneBook: function (data) { //print the phone book table
    console.log(data);
    var phoneTableBody = '';
    $.each(data, function (i, row) { // loop for each phone row
      console.log(row);
      phoneTableBody += '<tr>';
      phoneTableBody += '<td>' + row['id'] + '</td>';
      $.each(formDetails, function (i, val) {  // loop for each row details
        phoneTableBody += '<td>' + row[val] + '</td>';
      });
      phoneTableBody += '<td><button onclick="PhoneModel.edit(' + row['id'] + ')" type="button" class"btn btn-primary">edit</button></td>';
      phoneTableBody += '</tr>';
    });
    $('#phoneTable tbody').html(phoneTableBody);
  },
  printOrderOptions: function () {  //print options for select rows order
    var options = '<option value="">No sort</option>';
    $.each(formDetails, function (i, val) {
      options += '<option value="' + val + '">' + val + '</option>';
    });
    $("#orderBy").html(options);
  },
  getFormDetails: function (details) { //get details from form by id
    var obj = {};
    $.each(details, function (i, val) {
      obj[val] = $('#' + val).val();
    });
    return obj;
  },
  fillFormDetails: function () {  // fill row details in edit form
    var obj = JSON.parse(sessionStorage.getItem('updatePhoneData'));
    var data = obj.data[0];
    console.log(obj);
    var formDetails = ['firstName', 'lastName', 'phone', 'email', 'id'];
    $.each(formDetails, function (i, val) { //update the edit form inputs
      console.log(val, data[val]);
      $('#' + val).val(data[val]);
    });
  },
  edit: function (id) {  //edit single row from table
    HttpService.getPhoneDetails(id);
  },
  add: function () { //add new row from table
    sessionStorage.setItem('updatePhoneData', null);
    window.location.href = 'phoneUpdate.html';
  }
};
