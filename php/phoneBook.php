<?php

  require_once 'inc/HelpFunctions.php'; //config help functions
  require_once 'inc/DbConfig.php'; //config the Pdo object

  $table = 'numbers';
  $query = "SELECT * FROM $table ";

  if (!empty($_GET['id'])) {
    $id = $_GET['id'];
    $searchQuery = "WHERE id='$id'";
  } else {
    $searchQuery = searchInPhoneBook();
  }

  function searchInPhoneBook() { //Build search query
    $searchQuery = 'WHERE 1 ';
    $details = ['firstName', 'lastName', 'phone', 'email']; //details for search query
    foreach ($details as $val) { //loop for build the search query
      if (!empty($_GET[$val])) {
        $searchQuery .= " AND $val  LIKE '%" . $_GET[$val] . "%'";
      }
    }
    if (!empty($_GET['orderBy'])) { //Order by detail
      $searchQuery .= ' ORDER BY ' . $_GET['orderBy'];
    }
    return $searchQuery;
  }

  try {
    $result = $pdo->query($query . $searchQuery); //send query to sql DB.
    $data = getResult($result);
  } catch (Exception $exc) {
    $error = $exc->getTraceAsString();
    printError($error);
  }

  function getResult($result) { //Get  search result
    $data = [];
    if ($result && $result->rowCount() > 0) { //Check the search result
      $data = $result->fetchAll(PDO::FETCH_ASSOC);
    }
    return $data;
  }

  printResponse($data, $query . $searchQuery);

