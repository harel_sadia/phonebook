<?php

  require_once 'inc/HelpFunctions.php'; //config help functions
  require_once 'inc/DbConfig.php'; //config the Pdo object

  $table = 'numbers';
  $details = ['firstName', 'lastName', 'phone', 'email']; //details for search query


  if (!empty($_POST['id'])) { // check request status (add/update) by id
    $query = buildUpdateQuery($table, $details);
  } else {
    $query = buildAddQuery($table, $details);
  }

  function buildUpdateQuery($table, $details) { // build the update query
    $query = "UPDATE $table SET ";
    $detailsQuery = '';
    foreach ($details as $val) { //loop to get the update details
      $detailsQuery .= ", $val='" . $_POST[$val] . "'";
    }
    $query .= trim($detailsQuery, ',') . " WHERE id='" . $_POST['id'] . "'";
    return $query;
  }

  function buildAddQuery($table, $details) { // build query for add new row
    $query = "INSERT INTO $table ";
    $cols = $values = '';
    foreach ($details as $val) { //loop to get the add details
      $cols .= "$val,";
      $values .= "'$_POST[$val]',";
    }
    $query .= '(' . trim($cols, ',') . ')';
    $query .= ' VALUES (' . trim($values, ',') . ')';
    return $query;
  }

  try {
    $result = $pdo->exec($query); // send query to sql DB.
    printResponse(['result' => $result], $query);
  } catch (Exception $exc) {
    $error = $exc->getTraceAsString();
    printError($error);
  }
