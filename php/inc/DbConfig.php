<?php

  define('USER_NAME', 'root');
  define('PASSWORD', '');
  define('PDO_CONFIG', 'mysql:host=localhost;dbname=phonebook;charset=utf8');

  try {
    $pdo = new PDO(PDO_CONFIG, USER_NAME, PASSWORD);
  } catch (Exception $ex) {
    printError('no connection to db!');
  }