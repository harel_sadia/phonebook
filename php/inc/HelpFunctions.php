<?php

  function printResponse($data, $query) {  // print data to client (query for debug)
    $response = ['data' => $data, 'query' => $query];
    print_r(json_encode($response));
    http_response_code(200);
  }

  function printError($error) { // print error to client
    http_response_code(400);
    print_r(json_encode(['error' => $error]));
  }
